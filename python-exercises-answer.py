def q1():
    seq = []
    for x in range(2000, 3201):
        if x % 7 == 0 and x % 5 != 0:
            seq.append(str(x))
    return seq

def q2(i):
    d = dict()
    for x in range(1, i+1):
        d[x] = x*x 
    return d

def q3(str_input):
    return str_input.split(','), tuple(str_input.split(','))

class StringMethods:
    
    def __init__(self):
        self.strng = ''

    def get_string(self):
        self.strng = str(input('Enter string: '))

    def print_string(self):
        print(self.strng)

def q5(i):
    if i == 0:
        return 1
    else:
        return i * q5(i-1)

import math

def t2_q1(inp):
    c = 50
    h = 30
    res = []

    seq = inp.split(',')
    for d in seq:
        res.append(round(math.sqrt((2*c*float(d))/h)))
    return res

def t2_q2(inp):
    seq = inp.split(',')
    seq.sort()
    return seq

def t2_q3(inp):
    res = []
    inp = inp.split(',')
    for b2 in inp:
        b10 = int(b2, 2)
        if b10 % 5 == 0:
            res.append(b2)
    return res

def t3_q1(inp):
    return inp == inp[::-1]

if __name__ == '__main__':
    a1 = q1()
    print(', '.join(a1))

    a2_input = int(input('Enter a number: '))
    a2 = q2(a2_input)
    print(a2)

    a3_input = input('Enter a comma-separated sequence of numbers: ')
    a3_1, a3_2 = q3(a3_input)
    print(a3_1, a3_2)

    strm = StringMethods()
    strm.get_string()
    strm.print_string()

    a5_input = int(input('Enter a number to return its factorial: '))
    a5 = q5(a5_input)
    print(a5)

    t2_a1_input = input('Enter a sequence of numbers comma-separated: ')
    t2_a1 = t2_q1(t2_a1_input)
    print(t2_a1)

    t2_q2_input = input('Enter a sequence of comma-separated strings: ')
    t2_a2 = t2_q2(t2_q2_input)
    print(t2_a2)

    t2_q3_input = input('Enter a sequence of comma-separated 4 digit binary numbers: ')
    t2_a3 = t2_q3(t2_q3_input)
    print(t2_a3)

    t3_q1_input = input('Enter a string to know if it is a palindrome: ')
    t3_a1 = t3_q1(t3_q1_input)
    print(t3_a1)